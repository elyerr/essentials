#!/usr/bin/env python3

import os
import shutil
import sys
import subprocess

INSTALL_DIR = "/usr/share/essentials"
INSTALL_BIN = "/usr/local/bin"
INSTALL_DESKTOP = "/usr/share/applications/essential.desktop"
DIR = os.path.join(os.getcwd(), "share")


def remove():
    shutil.rmtree(INSTALL_DIR)
    os.remove(INSTALL_DESKTOP)
    os.unlink(os.path.join(INSTALL_BIN, 'essential.py'))
    print("Essentials ha sido removido de su sistema.")


def create_desktop():
    with open(INSTALL_DESKTOP, mode='w') as file:
        file.write('[Desktop Entry]\n')
        file.write('Name=Essentials\n')
        file.write('GenericName= Essentials\n')
        file.write('Exec=essential.py\n')
        file.write('Terminal=false\n')
        file.write('Type=Application\n')
        file.write('Icon=essentials\n')
        file.write('Categories=Office,Internet,Network\n')
        file.write('Comment=Essentials')
        file.close()


def install():
    if (os.path.exists(INSTALL_DIR)):
        shutil.rmtree(INSTALL_DIR)
    os.mkdir(INSTALL_DIR)

    shutil.copy('app.py', INSTALL_DIR)
    os.chmod(os.path.join(INSTALL_DIR, 'app.py'), 0o755)
    shutil.copytree('modules', os.path.join(INSTALL_DIR, 'modules'))

    try:
        os.symlink(os.path.join(INSTALL_DIR, 'app.py'),
                   os.path.join(INSTALL_BIN, 'essential.py'))

    except FileExistsError as e:
        os.unlink(os.path.join(INSTALL_BIN, 'essential.py'))
        os.symlink(os.path.join(INSTALL_DIR, 'app.py'),
                   os.path.join(INSTALL_BIN, 'essential.py'))

    # verificar existencia del archivo
    if (not os.path.exists(os.path.join(INSTALL_DIR, 'env.py'))):
        shutil.copy('env.py.default', os.path.join(INSTALL_DIR, 'env.py'))
        os.chmod(os.path.join(INSTALL_DIR, 'env.py'), 0o644)
    create_desktop()
    print(f"Se a completado la instalaccion")


if __name__ == "__main__":
    if os.geteuid() != 0:
        try:
            command = f"sudo {sys.argv[0]} {sys.argv[1]}"
        except IndexError as e:
            command = f"sudo {sys.argv[0]}"
        finally:
            subprocess.run(command, shell=True)
            sys.exit(0)

    if len(sys.argv) > 1 and sys.argv[1] == '-r':
        remove()
        sys.exit()

    install()
