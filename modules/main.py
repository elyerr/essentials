 

import tkinter as tk
from tkinter import ttk
import os, sys, platform
from modules.core import Core
from modules.users import SearchUser
from modules.company import SearchCompany
from modules.igv import IGV

class main(Core):

    def __init__(self) -> None:
        current_os = platform.system()
        if (current_os == "Windows"):
            self.main()
            sys.exit()
        
        lock_file = '/tmp/essential.lock'
        if (os.path.exists(lock_file)):
           sys.exit()
        
        with open(lock_file, 'w') as f:
            f.write(str(os.getpid()))

        self.main()

        os.remove(lock_file)

    def main(self):
        root = tk.Tk()
        root.geometry("800x100+100+0")
        root.resizable(False, False)
        root.title("Calcular IGV")
        root.bind("<Key>", lambda eve: self.close_window(eve, root))

        tabs = ttk.Notebook(root)
        tabs.pack(expand=True, fill="x")

        IGV(root, tabs)
        SearchUser(root, tabs)
        SearchCompany(root, tabs) 

        root.mainloop()

