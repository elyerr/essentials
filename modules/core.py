 
import tkinter as tk 
import os 
 
class Core():
    
    @staticmethod
    def close_window(eve, root):
        if "Escape" == eve.keysym:
            root.destroy()

    @staticmethod
    def copy_entry(entry):
        """copia el contenido al portapapeles

        Args:
            entry (_type_): tk.Entry
        """
        entry.select_range(0, len(entry.get()))
        data = entry.get()
        entry.clipboard_clear()
        entry.clipboard_append(data)

    @staticmethod
    def copy_text(entry):
        """copia el contenido al portapapeles

        Args:
            entry (_type_): tk.Entry
        """
        entry.tag_add(tk.SEL, "0.1", tk.END)
        data = entry.get(0.1, tk.END)
        entry.clipboard_clear()
        entry.clipboard_append(data)
    
    @staticmethod
    def create_file(name, data):
        home = os.path.expanduser('~')
        file = f'{home}/{name}'
        
        with open(file, mode='a+', newline='', encoding="utf-8") as line:
            for row in data: 
                line.write(str(row))
                line.write('|')
            line.write('\n')
            line.close()

        
    