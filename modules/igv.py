 
import tkinter as tk 
from tkinter import ttk, messagebox
from modules.core import Core
  
class IGV(tk.Frame):

    def __init__(self, master, tab) -> None:
        super().__init__(master) 
        
        # total
        label_total = tk.Label(self, text="Monto total")
        label_total.pack(side="left", padx=5)
        
        self.input_total = tk.Entry(self)
        self.input_total.focus()
        self.input_total.pack(side="left", padx=5)
        self.input_total.bind("<Key>", lambda event: self.key_is_pressed(event))

        
        label_igv = tk.Label(self, text="IGV")
        label_igv.pack(side="left", padx=5)
        
        self.combo_igv = ttk.Combobox(self, values=[18, 10], state="readonly")
        self.combo_igv.pack(side="left", padx=5)
        self.combo_igv.current(0)
        
        self.combo_igv.bind('<<ComboboxSelected>>', lambda event:self.is_changed(event))
        
        # output
        label_output = tk.Label(self, text="Sub Total")
        label_output.pack(side="left", padx=5)
        
        self.input_subtotal = tk.Entry(self)
        self.input_subtotal.pack(side="left", padx=5)
        
        button_copy = tk.Button(self, text="Copiar valor")
        button_copy.config(command=lambda: Core.copy_entry(self.input_subtotal))
        button_copy.pack(side="left", padx=5)
        
        tab.add(self, text="Calcula IGV")
    
    def key_is_pressed(self, event):
        if (event.keysym == "Return" or event.keysym == "KP_Enter"): 
            self.calcular_total()
            
    def is_changed(self, event):
        if self.input_total.get() != "":
            self.calcular_total()
           
    def calcular_total(self):
        try: 
            total = float(self.input_total.get())
            igv = int(self.combo_igv.get()) / 100

            subtotal = float(total) / (100 / 100 + igv)
            self.input_subtotal.delete(0, tk.END)
            self.input_subtotal.insert(0, str(round(subtotal, 3)))

        except ValueError as e:
            messagebox.showwarning("Valor Incorreto", e)
            self.input_total.delete(0, tk.END)
