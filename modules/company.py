from env import Env  
import tkinter as tk
from modules.core import Core
import requests
from tkinter import messagebox

class SearchCompany(tk.Frame):

    def __init__(self, master, tab) -> None:
        super().__init__(master)  
       
        # variables
        label_ruc = tk.Label(self, text="RUC")
        label_ruc.pack(side="left", padx=5)
        
        self.input_ruc = tk.Entry(self)
        self.input_ruc.pack(side="left", padx=5)
        self.input_ruc.bind('<Key>', lambda event: self.search_user(event))
        
        self.copy_ruc = tk.Button(self, text="COPIAR", command=lambda: Core.copy_entry(self.input_ruc))
        self.copy_ruc.pack(side="left", padx=5) 


        label_user = tk.Label(self, text="RAZON SOCIAL")
        label_user.pack(side="left", padx=5)        
        self.data_user = tk.Text(self, width=50, height=3)
        self.data_user.pack(side="left", padx=5)
        
        copy_user = tk.Button(self, text="COPIAR", command= lambda : Core.copy_text(self.data_user))
        copy_user.pack(side="left", padx=2) 
 

        # data de salida 

        tab.add(self, text="BUSCAR EMPRESA")

    def call_api(self):
        response = requests.get(Env.api_uri_ruc(self.input_ruc.get()))

        if response.status_code == 200:
            data = response.json()
            self.data_user.delete(0.1, tk.END)
            user = f"{data['razonSocial']}"
            self.data_user.insert(tk.INSERT, user)

        elif response.status_code == 404:
            messagebox.showerror(
                title="Error", message="Asegurese que los datos son correctos")

        elif response.status_code == 401:
            messagebox.showerror(
                title="Authenticate", message="Genere un token en APIS PERU")
        else:
            messagebox.showerror(
                title="Error", message="No se puedo encontrar el usuario")

    def search_user(self, eve):
        if (eve.keysym == "Return" or eve.keysym == "KP_Enter"):
            self.call_api()
