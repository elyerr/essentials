import tkinter as tk 
from env import Env
import requests
from tkinter import messagebox
from modules.core import Core

class SearchUser(tk.Frame):

    def __init__(self, master, tab) -> None:
        super().__init__(master) 
        
        # variables 
        label_dni = tk.Label(self, text="DNI")
        label_dni.pack(side="left", padx=5)
        
        self.input_dni = tk.Entry(self)
        self.input_dni.pack(side="left", padx=5)
        self.input_dni.bind('<Key>', lambda event: self.search_user(event, self.input_dni.get()))
        
        self.button_copy_dni = tk.Button(self, text="COPIAR", command= lambda: Core.copy_entry(self.input_dni))
        self.button_copy_dni.pack(side="left", padx=5)

        label_user = tk.Label(self, text="USUARIO")
        label_user.pack(side="left", padx=5)
        
        self.data_user = tk.Entry(self)
        self.data_user.pack(side="left", padx=5, expand=True, fill="x")
        
        self.button_copy_user = tk.Button(self, text="COPIAR", command= lambda: Core.copy_entry(self.data_user))
        self.button_copy_user.pack(side="left", padx=5) 

        tab.add(self, text="BUSCAR USUARIO")
  
    def call_api(self):
            response = requests.get(Env.api_uri_dni(self.input_dni.get()))

            if response.status_code == 200:
                data = response.json()
                self.data_user.delete(0, tk.END)
                user = '{} {} {}'.format(data['nombres'], data['apellidoPaterno'], data['apellidoMaterno'])
                self.data_user.insert(tk.INSERT, user)                 
                Core.create_file('users', [
                    user,
                    data["dni"],
                    data["codVerifica"]
                    ])
            
            elif response.status_code == 404:
                messagebox.showerror(
                    title="Error", message="Asegurese que los datos son correctos")

            elif response.status_code == 401:
                messagebox.showerror(
                    title="Authenticate", message="Usuario sin llave de authenticacion")
            else:
                messagebox.showerror(
                    title="Error", message="No se puedo encontrar el usuario")

    def search_user(self, event, dni):
        if (event.keysym == "Return" or event.keysym == "KP_Enter"):
            self.call_api()